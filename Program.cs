﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Discord;
using discord_bot.components;
using Newtonsoft.Json;

namespace discord_bot
{
    class Program
    {
        static void Main(string[] args)
        {

            //init player with 500 gems
            Player init = Player.getPlayerByDiscordID("testUser2");
            Console.WriteLine(init.displayInfo());
            init.updateGems(200);
            Console.WriteLine(init.displayInfo());
            init.summonHero();
            Console.WriteLine(init.displayInfo());
            init.summonHero();
            Console.WriteLine(init.displayInfo());
        }
    }
}
