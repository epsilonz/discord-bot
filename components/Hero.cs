﻿using System;

namespace discord_bot.components
{
	/**
	* Hero class
	* Defined by rarity.
	* Max strength = 5000
	* Hero strength = 5000 * rarity
	* Change this class to support more robust hero design
	*/
	public class Hero
	{
		private double _strength;
		private const int MAX_STRENGTH = 5000;
		private string _heroID;
		private double _rarity; //0-1
		public Hero(double rarity)
		{
			_heroID = Guid.NewGuid().ToString();
			_strength = rarity * MAX_STRENGTH;
			_rarity = rarity;
		}

		public string getID()
        {
			return _heroID;
        }

		public double getStrength()
        {
			return _strength;
        }

		public double getRaity()
        {
			return _rarity;
        }

		public string toString()
        {
			return string.Format("strength: {0}, rarity: {1} }}",
				_strength, _rarity);
        }
	}	


}

