﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace discord_bot.components
{
    class db : DbContext
    {
        private static bool _created = false;
        public db()
        {
            if (!_created)
            {
                _created = true;
                Database.EnsureCreated();

            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder b)
        {
            b.UseSqlite(@"Data Source='F:\discord-bot-C#\discord-bot\Database\main.db'");
        }

        public DbSet<Player> players { get; set; }
        //public DbSet<Hero> heros { get; set; }
    }
}
