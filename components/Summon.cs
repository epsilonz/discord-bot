using System;
/**
 * 
 */
namespace discord_bot.components
{
    public class Summon
    {
        /**
         * Summon hero class,
         * random int 0-100,
         * <=5 SSR
         * <=10 SS
         * <=20 S
         * <=30 Unique
         * <=40 rare
         * <=60 normal
         */
        public static Hero summon()
        {
            Random rnd = new Random();
            int num = rnd.Next(0, 101);
            double rarity = 1 - (num / 100.0);
            return new Hero(rarity);
        }
    }
}
