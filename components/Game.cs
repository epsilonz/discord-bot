﻿using System;
using System.Collections.Generic;
using System.Text;

namespace discord_bot.components
{
    class Game
    {
        private Player _p;
        public Game(Player p)
        {
            _p = p;
        }

        private bool summon()
        {
            bool b = _p.summonHero();
            return b;
        }

    }
}
