﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace discord_bot.components
{
	class Player
	{
		private int playerID;
		
		private String _dicordID;
		private int _gems;
		private int _rating;
		private int _combatPower;
		private Dictionary<string, string> _myHeros;
		private String _heroData = "";


		private Player(String discordID)
		{
			_dicordID = discordID;
			_myHeros = new Dictionary<string, string>();
			_gems = 500;//init gems
			//Console.WriteLine($"gems = {_gems}");
		}
		
		private void loadHeros(string heroData)
        {
			_myHeros = JsonConvert.DeserializeObject<Dictionary<string, string>>(heroData);

		}

		//entity framework setup below
		public Player()
        {
			// entiy bind
        }

		public int getGems()
        {
			using(var context = new db())
            {
				var p = context.players.AsQueryable().
					Where(ply => ply.DiscordID == DiscordID).FirstOrDefault<Player>();
				return p.Gems;
			}
        }


		[Key]
		public int PlayerID
		{
			get; set;
		}

		public String DiscordID {
			get; set;
        }

		public int Gems { get; set; }

		public int Rating { get; set; }

		public int CombatPower { get; set; }

		public string HeroData { get; set; }

		

		private void initPlayer()
        {
			using (var data = new db())
            {
				data.players.Add(
					new Player(_dicordID)
                    {
						DiscordID = _dicordID,
						Gems = _gems,
						CombatPower = CombatPower,
						Rating = Rating,
						HeroData = ""
                    }

					);
				data.SaveChanges();
            }
        }

		public void updateGems(int val)
        {
			using (var db = new db())
			{
				Console.WriteLine("discord ID = " + DiscordID);
				var p = db.players.AsQueryable().
					Where(ply => ply.DiscordID == DiscordID).FirstOrDefault<Player>();
				if (p != null)
				{
					p.Gems += val;
					db.SaveChanges();
				}
			}
        }

		public static Player getPlayerByDiscordID(string discordID)
        {
			using(var context = new db())
            {
				var p = context.players.AsQueryable().
						Where(ply => ply.DiscordID == discordID).FirstOrDefault<Player>();
				if (p != null)
                {
					if(p.HeroData != "")
                    {
						p.loadHeros(p.HeroData);
                    }
					return p;
                }
                else
                {
					Player ply = new Player(discordID);
					ply.initPlayer();
					return ply;
                }
            }
        }

		public bool summonHero()
        {
			using (var contxt = new db())
            {
				var p = contxt.players.AsQueryable().Where
					(ply => ply.DiscordID == DiscordID).FirstOrDefault<Player>();

				if (p.Gems > 300)
				{
					Hero h = Summon.summon();
					if (_myHeros == null) {
						_myHeros = new Dictionary<string, string>();
					}
					_myHeros.Add(h.getID(), h.toString());
					p.Gems -= 300;
					p.HeroData = JsonConvert.SerializeObject(_myHeros);
					contxt.SaveChanges();
					return true;
                }
                else
                {
					return false;
                }
			}
        }

		public string displayInfo()
        {
			using (var contxt = new db())
			{
				var p = contxt.players.AsQueryable().Where
					(ply => ply.DiscordID == DiscordID).FirstOrDefault<Player>();

				return string.Format("Player: {0}\nCurrent Gems: {1}\nCurrent Heros: {2}",
								 p.DiscordID, p.Gems, p.HeroData);
			}
        }
	}
}

